#!/bin/bash
set -x

ansible-playbook -i tests/inventory -c docker tests/test.yml | grep -q 'changed=0.*failed=0' && (echo 'Idempotence test: pass' && exit 0) || (echo 'Idempotence test: fail' && exit 1)
