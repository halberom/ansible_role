#!/bin/bash
set -x

role_dir=$(dirname $(pwd))
role_name=$(basename $role_dir)

# Look for a list of containers, default to 'centos'
[ "$CONTAINERS" ] || CONTAINERS='centos:7.2.1511'

# init inventory
> tests/inventory

for container in $CONTAINERS; do
    IFS=':' read -ra container_tag <<< "$container"
    name=${container_tag[0]}
    tag=${container_tag[1]}
    unset IFS

    docker run -dt --name $name $name:$tag /bin/bash

    # add to the inventory
    echo $name >> tests/inventory
done
