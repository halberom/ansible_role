## boilerplate ansible role for testing

This is a work-in-progress, intended to setup and try various different methods of testing an ansible role using a tool such as travis.

The requirements are
- check syntax
- perform a first run
- perform a second run checking for idempotence

Further requirements are the ability to test against multiple operating systems and/or versions

## TODO

- Using the default ubuntu:14.04 container fails as python isn't installed, so some post actions are required.
- Currently only one version of the same OS will work
- Look at using the ansible docker dynamic inventory
- Look at using the ansible container module
